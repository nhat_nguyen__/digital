<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'digital');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/*G?@gdB;5gSG;@tT_Zj)vdo57o::0nEqoOi!Yl=}0T0$66IrT*H,G:-n]g3if!M');
define('SECURE_AUTH_KEY',  'pH@j:j!%G^s>}b r *pmH~PRZ[(T$g^YN-!mq,9^HUEt!M]afQU0xS0Y8tO!2Oi/');
define('LOGGED_IN_KEY',    'pLO?c1/ha*L}5BhFVD.LTJ0[|[L3B2AdH&F3,Y%<Eza.d1qE A!jW*RNM/j~E^C^');
define('NONCE_KEY',        '<BCNAd*0y6S/sJ.wf/!_usiE#FVE0<z])a;<^ID?X!((DFi2:qXpLt6<FV2_v8 }');
define('AUTH_SALT',        '|c?JLlu2?$n?)`lQ{JMv-2$PfHDdIz2;VaV?BbUt7d.r:E$n`N!lpF19<<X9N|q`');
define('SECURE_AUTH_SALT', 'J,%%ForgeJPLRinj1Lc/N{[/d.YZ>Je_rw_Shj(K+A;kFovCj~5FyCbvFk`>|ZZC');
define('LOGGED_IN_SALT',   '2gyBFh;LQw+46l&(2w(EJ}p9ypT6,`$=8Tc&<3sa&7OF9fu{Bi7EXe?zBqL~hL&k');
define('NONCE_SALT',       '`xt$ keT7;r>i.5lBc%=@^P5`dU^Q3B|4}+Pi!!GG`E8&R)W,+qZ^;sL{p`Q}vf:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
